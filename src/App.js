//React imports
import React from 'react';
import { BrowserRouter as Router, Switch,Route } from 'react-router-dom';
//CSS File Import
import './App.css';
//Components import 
import Landing from './components/Landing';
import Register from './components/Register';
import Login from './components/Login';
import Dashboard from './components/Dashboard';


const App = () => {
  return (
    <Router>
        <Switch>
             <Route path='/' exact component={Landing} />
             <Route path='/register' exact component={Register} />
             <Route path='/login' exact component={Login} />
             <Route path='/dashboard' exact component={Dashboard} />    
        </Switch>
    </Router>
  );
}

export default App;
