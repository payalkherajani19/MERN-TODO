import React from 'react';

class Todoitem extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            editing: false,
            newVal: this.props.singleTodoItem.tasks

        }
    }

    render(){
        return(
            <div className="container">
                {
                    this.state.editing === true ? 
                    (
                        <div>
                             <input type="text" value={this.state.newVal} onChange={(e) => this.setState({newVal: e.target.value})} />
                             <button onClick={() => {
                                 this.props.editFunction(this.state.newVal,this.props.singleTodoItem.id)
                                this.setState({editing: false})
                                }
                             }>Update</button>     
                       </div>
                    ) : 
                    ( 
                    <div>  
                         {this.props.singleTodoItem.tasks}
                        <button onClick={() => this.setState({editing: true})}> Edit </button>
                    </div>
                    
                    )
                }
               
                <button onClick={() => this.props.deleteFunction(this.props.singleTodoItem.id)}>Delete</button>
            </div>
        )
    }
}

export default Todoitem;