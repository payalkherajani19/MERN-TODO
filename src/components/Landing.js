import React from 'react';
import todoimage from '../Images/todo.jpg';

//components
import Navbar from './Navbar';

const Landing = () => {
    return (
        <div className="container-fluid">
           <Navbar logout={false} />
           <img src={todoimage} className="img-fluid mb-2 image" alt="..."></img>
        </div>
    )
}

export default Landing;