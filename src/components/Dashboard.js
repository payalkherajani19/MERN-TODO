import React from 'react';
//import component
import Todoitem from './Todoitem';
import Navbar from './Navbar';

class Dashboard extends React.Component{

    constructor(props){
    super(props);
    this.state = {
        todos: [],
        tasks: "",
    }
    }

    //Functions defined

    inputHandler = (event) => {
        const inputValue = event.target.value;
        this.setState({
            tasks: inputValue
        })
    }

    addTodoFunction = () =>{
        const newTask = {
            id: Date.now(),
            tasks: this.state.tasks,
        }

        const newTodoItem = [...this.state.todos,newTask];
        this.setState({
            todos: newTodoItem,
            tasks: ""
        })
    }

    editTodo = (newVal,id) =>{
       const editTodoUpdate = this.state.todos.map((item) => {
           if(item.id === id){
              item.tasks=newVal
           }
           return item
           
       })
       this.setState({todos: editTodoUpdate});
    }
   
    deleteTodoItem = (id) => {
      const deleteTodoUpdate = this.state.todos.filter((item) => item.id!== id);
     this.setState({
         todos: deleteTodoUpdate
     })
    }


    render(){
        return (
            <div className="container-fluid">

                {/* Navbar */}
                <Navbar logout={true} />

               {/* Section */}
               <section>
                   {/* Add Todo */}
                   <div className="container mt-5">
                       <input type="text" onChange={this.inputHandler} value={this.state.tasks} />
                       <button onClick={this.addTodoFunction}>Add Todo</button>
                   </div>

                   <div  className="container mt-5">
                   {
                       this.state.todos.map((singleTodo) => {
                           return(
                           <div key={singleTodo.id}>
                               <Todoitem 
                               singleTodoItem={singleTodo}
                               editFunction={this.editTodo}
                               deleteFunction={this.deleteTodoItem}
                               />   
                           </div>
                           ) 
                       })
                   }
                   </div> 
               </section>
            </div>
        )
    }
   
}

export default Dashboard;