import React from 'react';
import Navbar from './Navbar';

//import image
import useradd from '../Images/adduser.svg'

const Register = () => {
    return (
        <div className="container-fluid ">
            <Navbar logout={false} />
            <div className="container d-flex">
                        <div className="register-div-1 container mt-5 ">
                            <img src={useradd} className="useraddimage d-none d-sm-block  " />
                        </div>

                        <div className="container mt-5 p-1 register-div-2">
                        <h1 className="mb-2 pb-2" >Register</h1>
                            <form>
                                <div className="mb-3">
                                    <input type="text" className="form-control" placeholder="Your Name" />
                                </div>

                                <div className="mb-3">
                                    <input type="email" className="form-control" placeholder="Your Email Address" />
                                </div>

                                <div className="mb-3">
                                    <input type="password" className="form-control" placeholder="Password" />
                                </div>

                                <div className="mb-3">
                                    <input type="password" className="form-control" placeholder="Confirm Password" />
                                </div>
                                
                                <button type="submit" class="btn btn-dark">Register</button>
                        </form>
                   </div>
            </div>  
        </div>
    )
}

export default Register;