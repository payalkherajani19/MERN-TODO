import React from 'react';
//component
import Navbar from './Navbar';

const Login = () => {
    return (
        <div className="container-fluid ">
           <Navbar logout={false} />
            <h1>Login</h1>


            <form>

                <div class="mb-3">
                    <input type="email" class="form-control" placeholder="Your Email Address" />
                </div>

                <div className="mb-3">
                    <input type="password" className="form-control" placeholder="Password" />
                </div>
 
                <button type="submit" class="btn btn-primary">Login</button>
          </form>
        </div>
    )
}



export default Login;