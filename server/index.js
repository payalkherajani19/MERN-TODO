const express = require('express');
const app = express();   //app is object of express
const PORT = process.env.PORT || 5000

//Connecting MongoDB
const connectDB = require('./config/db');
connectDB();

//Init Middleware
app.use(express.json({extended: false}));

//Routing in Express
app.use('/api/user',require('./routes/users'));
app.use('/api/auth',require('./routes/auth'));
app.use('/api/todo',require('./routes/todo'));

app.listen(PORT, () => {
    console.log(`Server started at ${PORT}`)
  });