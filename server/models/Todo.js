const mongoose = require('mongoose');

const TodoSchema = new mongoose.Schema({
 todos: [
     {
        tasks: {
            type: String,
            required: true
        },
        isCompleted: {
            type: Boolean,
            default: false
        },
        date: {
            type: Date,
            default: Date.now
        }
     }
 ]
});

module.exports = Todo = mongoose.model('todo',TodoSchema);