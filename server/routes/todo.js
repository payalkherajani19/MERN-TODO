const express = require('express');
const router = express.Router();
//Model
const Todo = require('../models/Todo');


// @route   GET api/todo
// @desc    get all todos
// @access  Public
router.get('/',async(req,res) => {
    try {
        const todos = await Todo.find();
        res.json(todos);
    } catch (error) {
        console.error(error.message);
        res.status(500).send('Server Error');
    }
});

// @route   POST api/todo
// @desc    add todos
// @access  Public

router.post('/',async(req,res) => {

    try {
        //Call model
        const todo = await Todo.find();
        //Instating new todo
        const newTodo = {
           tasks: req.body.todos.tasks,
           isCompleted: req.body.todos.isCompleted
        }
        console.log(newTodo);
        res.send('Checking');
        // await todo.save();
        // res.json(todo.todos);

    } catch (error) {
        console.error(error.message);
        res.status(500).send('Server Error');
    }

})

module.exports = router;