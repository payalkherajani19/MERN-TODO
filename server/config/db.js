const mongoose = require('mongoose');
const config = require('config');
const connectionURL = config.get('mongoURI');

//function for connection 
const connectDB = async() => {
    try {
        await mongoose.connect(connectionURL,{ useNewUrlParser: true,useUnifiedTopology: true,useCreateIndex: true,useFindAndModify: false });
        console.log('MongoDB Connected');
    } catch (error) {
        console.log(error.message)
        process.exit(1) //EXIT THE PROCESS WITH FAILURE
    }
}

module.exports = connectDB